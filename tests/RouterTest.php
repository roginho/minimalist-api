<?php

namespace Minimalist\Api\Test;

use Minimalist\Router\Router;
use PHPUnit\Framework\TestCase;

class RouterTest extends TestCase
{

    public function test_router()
    {

        $router = new Router('GET', '/');
        $router->get('/', function () {
            return true;
        });
        $result = $router->handler();
        $this->assertTrue($result());
    }
}
