<?php

namespace Minimalist\Api\Test;

use Minimalist\Router\Router;
use PHPUnit\Framework\TestCase;

class AuthTest extends TestCase
{

    public function test_login()
    {

        $router = new Router('POST', '/');
        $router->post('/auth/login', '\Minimalist\Api\Controllers\AuthController::login');
        $result = $router->handler();
        $this->assertTrue($result());
    }
}
