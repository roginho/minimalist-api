<?php

namespace Minimalist\Api\Controllers;

use Exception;
use Minimalist\Api\Http\Request;
use Minimalist\Api\Http\Response;
use Minimalist\Api\Models\User;
use Minimalist\Api\Services\GenerateTokenJwtUser;

class AuthController extends Controller
{
    public function login()
    {
        try{
            $dados = Request::body();
            $this->validateData($dados);
            $user = new User();
            $user->setEmail($dados['email']);
            $user->setUsername($dados['name']);

            $generateTokenJwtUser = new GenerateTokenJwtUser($user);
            $token = $generateTokenJwtUser->getToken();
            Response::send(200, $token);
        }catch(Exception $e){
            Response::send($e->getCode(), $e->getMessage());
        }
    }

    private function validateData(?array $dados)
    {
        if(empty($dados)){
            throw new \Exception('Login and/or password not provided.', 404);
        }
        return true;
    }
}
