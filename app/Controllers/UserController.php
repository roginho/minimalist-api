<?php

namespace Minimalist\Api\Controllers;

use Minimalist\Api\Http\Request;
use Minimalist\Api\Http\Response;

class UserController
{
    public function index()
    {
        return "index users";
    }

    public function store()
    {
        $dados = Request::body();
        if(empty($dados)){
            Response::send(400, [
                'invalid' =>  'Dados inválidos',
            ]);

        }
        Response::send(201, [
            'name' => $dados['name'],
            'email' => $dados['email'],
            'password' => $dados['password'],

        ]);
    }

    public function update(array $params)
    {
        return "update user id ".$params[1];
    }

    public function show(array $params)
    {
        return "show user id ".$params[1];
    }

}
