
<?php

namespace Minimalist\Api\Services;

use Minimalist\Api\Models\User;
use Minimalist\Jwt\Jwt;

class GenerateTokenJwtUser
{

    private ?string $token;

    public function __construct(User $user)
    {
        $jwt = new Jwt();
        $headers = array('alg'=>'HS256','typ'=>'JWT');
        $payload = array('sub'=>'1','name'=>$user->getUsername(), 'email'=>$user->getEmail(), 'exp'=>(time() + 60));
        $secret = "secret";
        $this->token = $jwt->generateJwt($headers, $payload, $secret);

    }

    public function getToken(): string
    {
        return $this->token;
    }
}
