<?php

// defino o método http e a url amigável
$method = $_SERVER['REQUEST_METHOD'];
//$url = !empty($_SERVER['REQUEST_URI']) ? explode('/',$_SERVER['REQUEST_URI']) : [];
$path = !empty($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '/';
//$path = !empty($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : '/';
// instancio o Router
$router = new \Minimalist\Router\Router($method, $path);


// registro as rotas
$router->get('/', function () {
    return 'Minimalist API';
});

$router->post('/auth/login', '\Minimalist\Api\Controllers\AuthController::login');
$router->post('/auth/logout', '\Minimalist\Api\Controllers\AuthController::logout');

$router->get('/users', '\Minimalist\Api\Controllers\UserController::index');
$router->post('/users', '\Minimalist\Api\Controllers\UserController::store');
$router->get('/users/{id}', '\Minimalist\Api\Controllers\UserController::show');
$router->put('/users/{id}', '\Minimalist\Api\Controllers\UserController::update');

$router->post('/login', '\Minimalist\Api\Controllers\AuthController::login');

// faço o router encontrar a rota que o usuário acessou
$result = $router->handler();

// se retornar false, dou um erro 404 de página não encontrada
if (!$result) {
    http_response_code(404);
    echo 'Página não encontrada!';
    die();
}

// verifico se é uma função anônima
if ($result instanceof Closure) {
    // imprimo a página atual
    echo $result($router->getParams());

// se não for uma função anônima e for uma string
} elseif (is_string($result)) {
    // eu quebro a string nos dois-pontos, dois::pontos
    // transformando em array
    $result = explode('::', $result);

    // instancio o controller
    $class= $result[0];
    $controller = new $class;
    // guardo o método a ser executado (em um controller ele se chama action)
    $action = $result[1];

    // finalmente executo o método da classe
    echo $controller->$action($router->getParams());
}

